package ai.lentra.configurations.repository;

import ai.lentra.configurations.exceptions.FileUploadException;
import ai.lentra.configurations.model.BaseGrid;
import ai.lentra.configurations.model.MasterMappingConfiguration;

import java.util.List;

public interface FileHandlerRepository {

    interface DbKeys {
        String COLLECTION_MASTER_CONFIGURATION = "masterMappingConfiguration";
        String KEY_INSTITUTION_ID = "institutionId";
        String KEY_PRODUCT = "lentraProduct";
        String KEY_SOURCE_MASTER_NAME = "masterName";
        String KEY_ACTIVE = "active";
        String kEY_MASTER_NAME = "masterName";
    }

    void saveMasterConfiguration(MasterMappingConfiguration masterMappingConfiguration) throws FileUploadException;

    boolean saveMasterData(List<BaseGrid> masterData, MasterMappingConfiguration masterMappingConfiguration);

    MasterMappingConfiguration getMasterConfiguration(String institutionId, String product, String masterName) throws FileUploadException;

    boolean isMasterConfigurationAvailable(String masterName, String institutionId, String product);

    int findMasterVersion(MasterMappingConfiguration masterMappingConfiguration) throws ClassNotFoundException;
}
