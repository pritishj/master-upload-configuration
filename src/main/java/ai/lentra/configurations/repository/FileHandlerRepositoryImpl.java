package ai.lentra.configurations.repository;

import ai.lentra.configurations.exceptions.FileUploadException;
import ai.lentra.configurations.model.BaseGrid;
import ai.lentra.configurations.model.MasterMappingConfiguration;
import ai.lentra.configurations.service.FileUploadManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Repository
public class FileHandlerRepositoryImpl implements FileHandlerRepository {

    private final MongoTemplate mongoTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileHandlerRepositoryImpl.class);

    public FileHandlerRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void saveMasterConfiguration(MasterMappingConfiguration masterMappingConfiguration) throws FileUploadException {
        try {
            Criteria criteria = Criteria.where(DbKeys.KEY_ACTIVE).is(true);
            criteria.and(DbKeys.KEY_INSTITUTION_ID).is(masterMappingConfiguration.getInstitutionId());
            criteria.and(DbKeys.KEY_PRODUCT).is(masterMappingConfiguration.getLentraProduct());
            criteria.and(DbKeys.KEY_SOURCE_MASTER_NAME).is(masterMappingConfiguration.getMasterName());
            Query query = Query.query(criteria);
            Update update = new Update();
            update.set(DbKeys.KEY_ACTIVE, false);
            mongoTemplate.updateMulti(query, update, MasterMappingConfiguration.class);
            mongoTemplate.insert(masterMappingConfiguration);
        } catch (Exception exception) {
            throw new FileUploadException(String.format("Error occurred while saving saveMasterMappingConfiguration with probable cause [{%s}]", exception.getMessage()));
        }

    }

    @Override
    public MasterMappingConfiguration getMasterConfiguration(String institutionId, String product, String masterName) throws FileUploadException {
        Criteria criteria = Criteria.where(DbKeys.KEY_ACTIVE).is(true);
        criteria.and(DbKeys.KEY_INSTITUTION_ID).is(institutionId);
        criteria.and(DbKeys.KEY_PRODUCT).is(product);
        criteria.and(DbKeys.KEY_SOURCE_MASTER_NAME).is(masterName);
        Query query = Query.query(criteria);
        return mongoTemplate.findOne(query, MasterMappingConfiguration.class);
    }

    @Override
    public boolean isMasterConfigurationAvailable(String masterName, String institutionId, String product) {
        Criteria criteria = Criteria.where(DbKeys.KEY_INSTITUTION_ID).is(institutionId);
        criteria.and(DbKeys.kEY_MASTER_NAME).is(masterName);
        criteria.and(DbKeys.KEY_PRODUCT).is(product);
        criteria.and(DbKeys.KEY_ACTIVE).is(true);
        Query query = Query.query(criteria);
        long count = mongoTemplate.count(query, DbKeys.COLLECTION_MASTER_CONFIGURATION);
        return count > 0;
    }

    @Override
    public int findMasterVersion(MasterMappingConfiguration masterMappingConfiguration) throws ClassNotFoundException {
        Criteria criteria = Criteria.where(DbKeys.KEY_INSTITUTION_ID).is(masterMappingConfiguration.getInstitutionId());
        criteria.and(DbKeys.KEY_PRODUCT).is(masterMappingConfiguration.getLentraProduct());
        criteria.and(DbKeys.KEY_ACTIVE).is(true);
        BaseGrid baseGrid = (BaseGrid) mongoTemplate.findOne(Query.query(criteria), Class.forName(masterMappingConfiguration.getClassNameWithPackage()));
        if (baseGrid != null) {
            return baseGrid.getVersion();
        }
        return 0;
    }

    @Override
    public boolean saveMasterData(List<BaseGrid> masterData, MasterMappingConfiguration masterMappingConfiguration) {
        try {

            if (CollectionUtils.isEmpty(masterData)) {
                return false;
            }

            Criteria criteria = Criteria.where(DbKeys.KEY_INSTITUTION_ID).is(masterMappingConfiguration.getInstitutionId());
            criteria.and(DbKeys.KEY_ACTIVE).is(true);
            criteria.and(DbKeys.KEY_PRODUCT).is(masterMappingConfiguration.getLentraProduct());
            Query query = Query.query(criteria);

            Update update = new Update();
            update.set(DbKeys.KEY_ACTIVE, false);
            mongoTemplate.updateMulti(query, update, Class.forName(masterMappingConfiguration.getClassNameWithPackage()), masterMappingConfiguration.getCollectionName());
            mongoTemplate.insert(masterData, masterMappingConfiguration.getCollectionName());
            return true;

        } catch (Exception ex) {
            LOGGER.error("Error occurred while inserting master data ", ex);
            return false;

        }
    }

}
