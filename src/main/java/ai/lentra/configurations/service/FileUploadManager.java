package ai.lentra.configurations.service;

import ai.lentra.configurations.exceptions.FileUploadException;
import ai.lentra.configurations.model.MasterMappingConfiguration;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadManager {

    void uploadMaster(MultipartFile multipartFile, String institutionId, String masterName, String product);

    void saveMasterConfiguration(MasterMappingConfiguration masterMappingConfiguration);
}
