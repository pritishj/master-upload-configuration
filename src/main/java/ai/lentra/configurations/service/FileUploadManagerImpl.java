package ai.lentra.configurations.service;

import ai.lentra.configurations.exceptions.FileUploadException;
import ai.lentra.configurations.model.BaseGrid;
import ai.lentra.configurations.model.MasterMappingConfiguration;
import ai.lentra.configurations.repository.FileHandlerRepository;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@Service
public class FileUploadManagerImpl implements FileUploadManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadManagerImpl.class);

    private static final List<String> ALLOWED_FILE_TYPES = Arrays.asList("text/csv");

    private static final String DIRECTORY_TO_STORE_FILE = "/tmp";

    private final FileHandlerRepository fileHandlerRepository;

    public FileUploadManagerImpl(FileHandlerRepository fileHandlerRepository) {
        this.fileHandlerRepository = fileHandlerRepository;
    }

    @Override
    public void uploadMaster(MultipartFile multipartFile, String institutionId, String masterName, String product) {
        try {
            // basic validations on file.
            validateFile(multipartFile, institutionId, masterName, product);

            // Saving file to local directory for uploading in db.
            String fileName = multipartFile.getOriginalFilename() == null ? masterName + "-" + product : multipartFile.getOriginalFilename();
            fileName = fileName.replaceAll("\\s", "-");
            File file = new File(DIRECTORY_TO_STORE_FILE, fileName);
            FileUtils.writeByteArrayToFile(file, multipartFile.getBytes());

            MasterMappingConfiguration masterMappingConfiguration = fileHandlerRepository.getMasterConfiguration(institutionId, product, masterName);
            if (masterMappingConfiguration == null) {
                throw new FileUploadException(String.format("Master Configuration not found for master %s", masterName));
            }

            List<BaseGrid> list = transformCsvToList(file, masterMappingConfiguration.getClassNameWithPackage(), masterMappingConfiguration);
            if (list != null && list.size() > 0) {
                int version = fileHandlerRepository.findMasterVersion(masterMappingConfiguration);
                final int newVersion = version == 0 ? 1 : (version + 1);
                list.forEach(o -> {
                    o.setInstitutionId(institutionId);
                    o.setLentraProduct(product);
                    o.setVersion(newVersion);
                });
                fileHandlerRepository.saveMasterData(list, masterMappingConfiguration);
            }
            // deleting after uploading in db.
            FileUtils.deleteQuietly(file);

        } catch (Exception e) {
            LOGGER.info("Error Message is :: {}", e.getMessage());
            LOGGER.error("Exception", e);
        }
    }

    @Override
    public void saveMasterConfiguration(MasterMappingConfiguration masterMappingConfiguration) {
        try {
            fileHandlerRepository.saveMasterConfiguration(masterMappingConfiguration);
        } catch (FileUploadException e) {
            e.printStackTrace();
        }
    }

    private void validateFile(MultipartFile multipartFile, String institutionId, String masterName, String product) throws FileUploadException {
        if (multipartFile == null || multipartFile.isEmpty()) {
            throw new FileUploadException("File is Null/Empty.");
        }
        if (!ALLOWED_FILE_TYPES.contains(multipartFile.getContentType())) {
            throw new FileUploadException("File Type is not allowed.");
        }
        if (!fileHandlerRepository.isMasterConfigurationAvailable(masterName, institutionId, product)) {
            throw new FileUploadException(String.format("Configuration not found for institutionId = \"%s\" where master name is \"%s\" and Product is \"%s\"", institutionId, masterName, product));
        }
    }

    @SuppressWarnings("ALL")
    private List<BaseGrid> transformCsvToList(File file, String classNameWithPackage, MasterMappingConfiguration masterMappingConfiguration) throws IOException {
        CSVReader fileReader = null;
        try {
            fileReader = new CSVReader(new FileReader(file));
            CsvToBean csvToBean = new CsvToBean();
            HeaderColumnNameMappingStrategy strategy = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Class.forName(masterMappingConfiguration.getClassNameWithPackage()));
            List data = csvToBean.parse(strategy, fileReader);
            return data;
        } catch (Exception ex) {
            LOGGER.info("Something went wrong ", ex);
        } finally {
            if (fileReader != null) {
                fileReader.close();
            }
        }
        return null;
    }

}
