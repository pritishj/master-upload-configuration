package ai.lentra.configurations.configurations;

import com.mongodb.client.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;

import java.net.UnknownHostException;

@Configuration
public class AppMongoConfig {

    private final MongoClient mongoClient;

    public AppMongoConfig(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    @Bean
    public MongoTemplate mongoTemplateFraud() throws UnknownHostException {

        MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, "sslcoredev");
        ((MappingMongoConverter)mongoTemplate.getConverter()).setTypeMapper(new DefaultMongoTypeMapper(null));//removes _class
        return mongoTemplate;
    }

}