package ai.lentra.configurations.exceptions;

public class FileUploadException extends Exception {

    public FileUploadException() {
        super();
    }

    public FileUploadException(String s) {
        super(s);
    }

    public FileUploadException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public FileUploadException(Throwable throwable) {
        super(throwable);
    }

    protected FileUploadException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
