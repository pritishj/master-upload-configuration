package ai.lentra.configurations.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@Document(collection = "PricingMaster")
public class PricingGrid extends BaseGrid {

    private String product;

    private String customerProfile;

    private String propertyUsage;

    private String loanPurpose;

    private String incomeProgram;

    private String cibilBracket;

    private String loanSlab;

    private double raacRate;

    private double processingFees;

}
