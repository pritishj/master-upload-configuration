package ai.lentra.configurations.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Created by Pritish on 29/12/2020
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "masterMappingConfiguration")
public class MasterMappingConfiguration {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sMasterName")
    private String masterName;

    @JsonProperty("sCollectionName")
    private String collectionName;

    @JsonProperty("sClassNameWithPackage")
    private String classNameWithPackage;

    @JsonProperty("sProduct")
    private String lentraProduct;

    @JsonProperty("active")
    private boolean active = true;

    @JsonIgnore
    private Date insertDate = new Date();
}