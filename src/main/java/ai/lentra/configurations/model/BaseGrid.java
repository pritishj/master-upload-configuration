package ai.lentra.configurations.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class BaseGrid {

    @JsonProperty
    boolean active = true;

    @JsonProperty
    private String institutionId;

    @JsonProperty
    private String lentraProduct;

    @JsonProperty
    private int version;
}
