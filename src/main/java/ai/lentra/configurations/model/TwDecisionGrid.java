package ai.lentra.configurations.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class TwDecisionGrid extends BaseGrid {

    private String negativeArea;
    private String policyExclusions;
    private String ogl;
    private String scorecardDecision;
    private String dedupe;
    private String hunter;
    private String panVerification;
    private String finalDecision;
    private String newDecisionReasons;


}
