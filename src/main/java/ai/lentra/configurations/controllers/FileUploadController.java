package ai.lentra.configurations.controllers;


import ai.lentra.configurations.exceptions.FileUploadException;
import ai.lentra.configurations.model.MasterMappingConfiguration;
import ai.lentra.configurations.service.FileUploadManager;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
public class FileUploadController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);


    private final FileUploadManager fileUploadManager;

    public FileUploadController(FileUploadManager fileUploadManager) {
        this.fileUploadManager = fileUploadManager;
    }

    @PostMapping(value = "uploadMaster")
    @ResponseBody
    public Object uploadMaster(@RequestParam(value = "file") final MultipartFile multipartFile, @RequestParam String institutionId, @RequestParam String masterName, @RequestParam String product) {

        LOGGER.debug("uploadMaster Processing Started.");

        fileUploadManager.uploadMaster(multipartFile, institutionId, masterName, product);

        return null;
    }

    @PostMapping(value = "saveMasterConfiguration")
    @ResponseBody
    public Object uploadMasterConfiguration(@RequestBody MasterMappingConfiguration masterMappingConfiguration) {

        LOGGER.debug("uploadMaster Processing Started.");

        fileUploadManager.saveMasterConfiguration(masterMappingConfiguration);

        return null;
    }


}
